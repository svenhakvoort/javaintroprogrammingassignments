/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week3_1;

import java.text.DecimalFormat;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class Animal {

    private final String name;
    private final String movement;
    private final int max_age;
    private final double max_speed;
    private int age;

    public Animal(String name, int age, String movement, int max_age, double max_speed) {
        this.name = name;
        this.max_age = max_age;
        this.max_speed = max_speed;
        this.age = setAge(age);
        this.movement = movement;
    }

    public int setAge(int age) {
        if (age > max_age) {
            System.out.println("Error: maximum age of " + this.name + " is " + this.max_age + " years. Please give new values");
            return -1;
        }
        return age;
    }

    public int getAge() {
        return age;
    }

    /**
     * returns the name of the animal
     * @return name the species name
     */
    public String getName() {
        return name;
    }
    
    /**
     * returns the movement type
     * @return movementType the way the animal moves
     */
    public String getMovementType() {
        return movement;
    }
    
    /**
     * returns the speed of this animal
     * @return speed the speed of this animal
     */
    public String getSpeed() {
        DecimalFormat df = new DecimalFormat("#.0");
        return df.format(max_speed * (0.5 + (0.5 * ((double) (max_age - age) / max_age))));
    }

    public String toString() {
        if (name.matches("^[aeiouyAEIOUY].*")) {
            return "An " + name + " of age " + age + " moving in " + movement + " at " + getSpeed() + " km/h";
        } else {
            return "A " + name + " of age " + age + " moving in " + movement + " at " + getSpeed() + " km/h";
        }
    }
    
}
