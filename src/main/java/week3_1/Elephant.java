/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week3_1;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class Elephant extends Animal {

    public Elephant(String name, int age) {
        super(name, age, "thunder", 86, 40);
    }
}
