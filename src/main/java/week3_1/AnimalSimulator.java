/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week3_1;

import com.sun.xml.internal.ws.util.StringUtils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class AnimalSimulator {
    public static void main(String[] args) {
        AnimalSimulator anSim = new AnimalSimulator();
        anSim.start(args);
    }


    private void start(String[] args) {
        List<String> animals = getSupportedAnimals();
        Animal animal = null;
        if (args.length == 0 || Objects.equals(args[0], "help") || args.length == 1) {
            System.out.println("Usage: java AnimalSimulator <Species age Species age ...>");
            System.out.println("Supported species (in alphabetical order):");
            for (int i = 0; i < animals.size(); i++) {
                System.out.println(i + 1 + ": " + StringUtils.capitalize(animals.get(i)));
            }
        } else {
            for (int i = 0; i < args.length; i += 2) {
                String arg = args[i];
                int age = Integer.parseInt(args[i + 1]);
                switch (arg.toLowerCase()) {
                    case "elephant":
                        animal = new Elephant(arg, age);
                        break;
                    case "horse":
                        animal = new Horse(arg, age);
                        break;
                    case "house mouse":
                    case "housemouse":
                    case "mouse":
                        animal = new HouseMouse(arg, age);
                        break;
                    case "tortoise":
                        animal = new Tortoise(arg, age);
                        break;
                    default:
                        System.out.println("Error: animal species " + arg +
                                " is not known. run with single parameter" +
                                " \"help\" to get a listing of available species. Please give new values");
                }
                if (animal != null && animal.getAge() >= 0) {
                    System.out.println(animal);
                }
            }
            //start processing command line arguments
        }
    }

    /**
     * returns all supported animals as List, alhabetically ordered
     *
     * @return supportedAnimals the supported animals
     */
    public List<String> getSupportedAnimals() {
        List<String> animals = Arrays.asList("elephant", "horse", "mouse", "tortoise");
        Collections.sort(animals);
        return animals;
    }
}
