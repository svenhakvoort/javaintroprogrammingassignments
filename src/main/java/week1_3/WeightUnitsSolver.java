/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week1_3;

import java.util.Objects;
import java.util.Scanner;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class WeightUnitsSolver {
    private static Scanner keyboard = new Scanner(System.in);
    /**
     * main to be used for testing purposes
     * @param args 
     */
    public static void main(String[] args) {
        WeightUnitsSolver wus = new WeightUnitsSolver();
        boolean keepGoing = true;
        while (keepGoing) {
            try {
                System.out.print("Enter weight to convert in grams: ");
                String input = keyboard.nextLine();
                if (Objects.equals(input.toLowerCase(), "stop")) {
                    keepGoing = false;
                    break;
                }
                double convertValue = Double.parseDouble(input);
                System.out.println(wus.convertFromGrams(convertValue));
                keepGoing = false;
            } catch (NumberFormatException ex) {
                System.out.println("This is no number! Try again or type 'stop' to exit.");
            }
        }
    }
    
    /**
     * will return the number of Pounds, Ounces and Grams represented by this quantity of grams, 
     * encapsulated in a BritishWeightUnits object.
     * @param grams
     * @return a BritishWeightUnits instance
     * @throws IllegalArgumentException when the Grams quantity is 
     */
    public BritishWeightUnits convertFromGrams(double grams) {
        double pounds = (grams - (grams % 454));
        grams = grams - pounds;
        pounds = pounds / 454;

        double ounces = (grams - (grams % 28));
        grams = grams - ounces;
        ounces = ounces / 28;

        return new BritishWeightUnits(pounds, ounces, grams - pounds - ounces);
    }
}
