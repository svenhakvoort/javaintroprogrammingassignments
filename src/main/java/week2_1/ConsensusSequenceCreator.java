/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week2_1;

import com.sun.deploy.util.StringUtils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class ConsensusSequenceCreator {
    HashMap<String, String> iupacCodes = new HashMap<String, String>() {{
        put("AT", "W");
        put("CG", "S");
        put("AC", "M");
        put("GT", "K");
        put("AG", "R");
        put("CT", "Y");
        put("CGT", "B");
        put("AGT", "D");
        put("ACT", "H");
        put("ACG", "V");
        put("ACGT", "N");
    }};

    /**
     * testing main.
     *
     * @param args: cmd arguments
     */
    public static void main(String[] args) {
        String[] sequences = new String[4];
        sequences[0] = "GAAT";
        sequences[1] = "GAAA";
        sequences[2] = "GATT";
        sequences[3] = "GAAC";

        ConsensusSequenceCreator csc = new ConsensusSequenceCreator();
        String consensus = csc.createConsensus(sequences, true);
        //consensus should equal "GAWH"
        consensus = csc.createConsensus(sequences, false);
        //consensus should equal "GA[A/T][A/T/C]"
        System.out.println(consensus);
    }

    /**
     * creates a consensus sequence from the given array of sequences.
     *
     * @param sequences the sequences to scan for consensus
     * @param iupac     flag to indicate IUPAC (true) or bracket notation (false)
     * @return consensus the consensus sequence
     */
    public String createConsensus(String[] sequences, boolean iupac) {

        StringBuilder consensus = new StringBuilder();
        for (int i = 0; i < sequences[0].length(); i++) {
            Set<String> partial = new HashSet<String>();

            for (String seq : sequences) {
                partial.add(seq.substring(i, i + 1));
            }

            TreeSet<String> sorted = new TreeSet<>(partial);

            if (sorted.size() > 1) {
                if (iupac) {
                    consensus.append(iupacCodes.get(StringUtils.join(sorted, "")));
                } else {
                    consensus.append("[").append(StringUtils.join(sorted, "/")).append("]");
                }
            } else {
                consensus.append(StringUtils.join(sorted, "/"));
            }

        }
        return consensus.toString();
    }
}
