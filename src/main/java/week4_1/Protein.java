/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week4_1;

import java.util.Comparator;
import java.util.HashMap;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class Protein implements Comparable<Protein> {
    HashMap<Character, Double> aminoAcids = new HashMap<Character, Double>() {{
        put('I', 131.1736);
        put('L', 131.1736);
        put('K', 146.1882);
        put('M', 149.2124);
        put('F', 165.1900);
        put('T', 119.1197);
        put('W', 204.2262);
        put('V', 117.1469);
        put('R', 174.2017);
        put('H', 155.1552);
        put('A', 89.0935);
        put('N', 132.1184);
        put('D', 133.1032);
        put('C', 121.1590);
        put('Q', 146.1451);
        put('E', 147.1299);
        put('G', 75.0669);
        put('P', 115.1310);
        put('S', 105.0930);
        put('Y', 181.1894);
    }};

    private final String name;
    private final String accession;
    private final String aminoAcidSequence;
    private GOannotation goAnnotation;

    /**
     * constructs without GO annotation;
     * @param name
     * @param accession
     * @param aminoAcidSequence 
     */
    public Protein(String name, String accession, String aminoAcidSequence) {
        this.name = name;
        this.accession = accession;
        this.aminoAcidSequence = aminoAcidSequence;
    }

    /**
     * construicts with main properties.
     * @param name
     * @param accession
     * @param aminoAcidSequence
     * @param goAnnotation 
     */
    public Protein(String name, String accession, String aminoAcidSequence, GOannotation goAnnotation) {
        this.name = name;
        this.accession = accession;
        this.aminoAcidSequence = aminoAcidSequence;
        this.goAnnotation = goAnnotation;
    }

    /**
     * provides a range of possible sorters, based on the type that is requested.
     * @param type
     * @return proteinSorter
     */
    public static Comparator<Protein> getSorter(SortingType type) {
        if (type == null) {
            throw new IllegalArgumentException("Not a valid sorting type, please use a valid sorting type");
        }
        switch (type) {
            case ACCESSION_NUMBER:
                return (protein, t1) -> String.CASE_INSENSITIVE_ORDER.compare(protein.accession, t1.accession);

            case GO_ANNOTATION:
                return (protein, t1) -> {

                    int comp = String.CASE_INSENSITIVE_ORDER.compare(protein.goAnnotation.getBiologicalProcess(),
                            t1.goAnnotation.getBiologicalProcess());
                    if (comp == 0) {
                        comp = String.CASE_INSENSITIVE_ORDER.compare(protein.goAnnotation.getCellularComponent(),
                                t1.goAnnotation.getCellularComponent());
                        if (comp == 0) {
                            comp = String.CASE_INSENSITIVE_ORDER.compare(protein.goAnnotation.getMolecularFunction(),
                                    t1.goAnnotation.getMolecularFunction());
                        }
                    }
                    return comp;
                };

            case PROTEIN_WEIGHT:
                return (protein, t1) -> Double.compare(t1.getMolWeight(), protein.getMolWeight());
            default:
                return (protein, t1) -> String.CASE_INSENSITIVE_ORDER.compare(protein.name, t1.name);
        }
    }

    @Override
    public int compareTo(Protein o) {
        return String.CASE_INSENSITIVE_ORDER.compare(this.name, o.name);
    }

    /**
     *
     * @return name the name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @return accession the accession number
     */
    public String getAccession() {
        return accession;
    }

    /**
     *
     * @return aminoAcidSequence the amino acid sequence
     */
    public String getAminoAcidSequence() {
        if (aminoAcidSequence.toUpperCase().matches("[GALMFWKQESPVICYHRNDT]")) {
            return aminoAcidSequence;
        } else {
            throw new IllegalArgumentException("Invalid amino acid sequence, please enter a valid sequence");
        }
    }

    public double getMolWeight() {
        double weight = 0;
        for (int i = 0; i < aminoAcidSequence.length(); i++) {
            weight += aminoAcids.get(aminoAcidSequence.charAt(i));
        }
        return weight;
    }

    /**
     *
     * @return GO annotation
     */
    public GOannotation getGoAnnotation() {
        return goAnnotation;
    }

    @Override
    public String toString() {
        return "Protein{" + "name=" + name + ", accession=" + accession + ", aminoAcidSequence=" + aminoAcidSequence + '}';
    }

}
